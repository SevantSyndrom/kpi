package me.sevant.kpi.config

import me.sevant.kpi.config.ConfigurationType.*
import ninja.leaping.configurate.commented.CommentedConfigurationNode
import ninja.leaping.configurate.hocon.HoconConfigurationLoader
import ninja.leaping.configurate.json.JSONConfigurationLoader
import ninja.leaping.configurate.loader.ConfigurationLoader
import ninja.leaping.configurate.xml.XMLConfigurationLoader
import ninja.leaping.configurate.yaml.YAMLConfigurationLoader
import org.yaml.snakeyaml.DumperOptions.FlowStyle
import java.io.*
import java.lang.RuntimeException
import java.net.URL
import java.util.concurrent.Callable

class Configuration : ConfigSection() {


    companion object {
        fun load(file : File, type : ConfigurationType? = null) : Configuration {
            val conf = Configuration()
            var loader : ConfigurationLoader<*>? = null

                    try {
                        val ext = file.extension

                        if(ext == "yml" || type == YML){
                            loader = YAMLConfigurationLoader.builder().setFlowStyle(FlowStyle.BLOCK).setIndent(2).setFile(file).build()
                        }else if(ext == "json" || type == JSON) {
                            loader = JSONConfigurationLoader.builder().setFile(file).build()
                        }else if(ext == "conf" || type == HOCON){
                            loader = HoconConfigurationLoader.builder().setFile(file).build()
                        }else if(ext == "xml" || type == XML) {
                            loader = XMLConfigurationLoader.builder().setFile(file).build()
                        }


                        if(loader == null) throw UnknownFileTypeException(file.name)

                        conf.loader = loader
                        conf.node = loader.load()

                    }catch (ex : IOException) {
                        ex.printStackTrace()
                    }


            return conf
        }

        fun load(webURL : URL, temp : File, type : ConfigurationType? = null, backup : File, timeOutC : Int = 10000, timeOutR : Int = 10000, props : Map<String, String> = hashMapOf(), overwrite : Boolean = true) : Configuration {
            if(backup.exists() && backup.length() > 0 && !overwrite){
                return load(backup, type)
            }

            if(temp.exists()) temp.delete()

            temp.createNewFile()

            val con = webURL.openConnection()
            con.connectTimeout = timeOutC
            con.readTimeout = timeOutR
            con.doInput = true
            con.useCaches = false
            props.forEach{
                con.setRequestProperty(it.key, it.value)
            }
            val reader = BufferedReader(InputStreamReader(con.getInputStream()))
            val writer = BufferedWriter(FileWriter(temp))

            val lines = reader.readLines()
            for(line in lines){
                writer.write(line)
                writer.write("\n")
            }
            reader.close()
            writer.close()
            if(temp.length() > 0 && temp.length() != backup.length())
            {
                if(backup.exists()) backup.delete()

                temp.copyTo(backup)
                temp.delete()
            }

            return Configuration.load(backup)
        }

        fun load(inputStream : InputStream, type: ConfigurationType) : Configuration{
            var loader : ConfigurationLoader<*>? = null
            val config = Configuration()
            val reader = BufferedReader(InputStreamReader(inputStream))

            val source = object : Callable<BufferedReader> {
                override fun call(): BufferedReader {
                    return reader
                }

            }

            loader = when(type){
                YML -> YAMLConfigurationLoader.builder().setSource(source).setFlowStyle(FlowStyle.BLOCK).setIndent(2).build()
                JSON -> JSONConfigurationLoader.builder().setSource(source).build()
                HOCON -> HoconConfigurationLoader.builder().setSource(source).build()
                XML -> XMLConfigurationLoader.builder().setSource(source).build()
            }

            config.loader = loader
            config.node = loader.load()
            reader.close()
            inputStream.close()
            return config
        }

    }
}


class UnknownFileTypeException(val fileName : String) : RuntimeException("Could not infer configuration loader for file: $fileName")

enum class ConfigurationType{
    YML,
    XML,
    HOCON,
    JSON
}