package me.sevant.kpi.menu.animation

import org.bukkit.Bukkit
import org.bukkit.plugin.Plugin
import org.bukkit.scheduler.BukkitRunnable
import java.util.*

abstract class AnimationController {

    var tickRate = 0
    protected var taskID = hashMapOf<UUID, Int>()
    var animatorTask : () -> Unit = {}

    fun registerTask(plugin : Plugin, uuid : UUID){
        if(tickRate == 0) return
        val t = Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, object : BukkitRunnable() {
            override fun run() {
               animatorTask()
            }

        }, 0L, tickRate.toLong())

    }
}