package me.sevant.kpi.menu

import me.sevant.kpi.menu.event.MenuClickEvent
import org.bukkit.entity.Player
import org.bukkit.event.EventHandler
import org.bukkit.event.Listener
import org.bukkit.event.inventory.InventoryClickEvent
import org.bukkit.event.inventory.InventoryType
import org.bukkit.plugin.Plugin

import java.util.*
import kotlin.collections.HashMap

class MenuManager(plugin : Plugin) : Listener{

    init {
        plugin.server.pluginManager.registerEvents(this, plugin)
    }
    private val menus = hashMapOf<String, Menu>()
    private val menuTitles = hashMapOf<String, String>()
    //private val playerMenus = hashMapOf<UUID, Menu>()


    fun hasName(name : String) : Boolean = menus.containsKey(name)

    fun getMenuByName(name : String) = menus[name]

    fun getAllMenus() = menus.values.toList()


    fun registerMenu(menu : Menu){
        this.menus[menu.name] = menu
        this.menuTitles[menu.name] = menu.title
    }

    @EventHandler
    fun onInventoryClick(event : InventoryClickEvent){
        if(menuTitles.containsValue(event.view.title)){
            val filtered = menuTitles.filterValues { it == event.view.title }
            var menu : Menu? = null
            filtered.forEach {
                menu = menus[it.key]
                val slot = menus[it.key]?.getSlot(event.slot) ?: return@forEach
                val e = MenuClickEvent(event.whoClicked as Player, menus[it.key]!!, event.inventory, slot, event.click)
                if(event.clickedInventory?.type == InventoryType.PLAYER){
                    if(menu!!.blockInput){
                        event.isCancelled = true
                    }
                    return
                }
                slot.execute(e)
                event.isCancelled = e.isCancelled
            }

        }
    }
}