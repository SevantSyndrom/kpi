package me.sevant.kpi.menu

import javafx.scene.paint.Material
import me.sevant.kpi.menu.event.MenuClickEvent
import org.bukkit.inventory.ItemStack

data class Slot(
    var menuItem: MenuItem = MenuItem()
) {

    var executors = linkedSetOf<MenuClickEvent.() -> Unit>()

    fun buildItemStack() : ItemStack{
        //TODO placeholder parser
        return menuItem.build()
    }

    fun execute(event : MenuClickEvent){
        executors.forEach {
            it.invoke(event)
        }
    }

    fun executor(executor : MenuClickEvent.() -> Unit){
        executors.add(executor)
    }
}

fun Menu.addSlot(index : Int, builder : Slot.() -> Unit) {
    this.setSlot(index, Slot().apply(builder))
}

@DslMarker
annotation class SlotDSL

@MenuDSL
fun Menu.slot(builder : Slot.() -> Unit) = Slot().apply(builder)

