package me.sevant.kpi.menu

import org.bukkit.Material
import org.bukkit.inventory.ItemStack


class MenuItem {

    var material = Material.AIR
    var displayName = ""
    var lore = listOf<String>()
    var amount = 1


    fun build() : ItemStack {
        val item = ItemStack(material, amount)
        item.itemMeta?.setDisplayName(displayName)
        item.itemMeta?.lore = lore
        return item
    }


}


@SlotDSL
fun Slot.menuItem(builder : MenuItem.() -> Unit) {
    this.menuItem = MenuItem().apply(builder)

}