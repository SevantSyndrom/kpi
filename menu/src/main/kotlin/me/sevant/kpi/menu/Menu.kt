package me.sevant.kpi.menu

import me.sevant.kpi.menu.event.MenuOpenEvent
import org.bukkit.Bukkit
import org.bukkit.entity.Player
import org.bukkit.inventory.Inventory
import java.io.Serializable

data class Menu (
            var name : String = "",
            var title : String = "",
            var rows : Int = 1,
            var close : Boolean = false,
            var blockInput : Boolean = false
            ){

    private val onOpenExecutors = linkedSetOf<MenuOpenEvent.() -> Unit>()
    private val slots = hashMapOf<Int, Slot>()

    fun setSlot(index : Int, slot : Slot?){
        if(slot == null) slots.remove(index)
        else slots[index] = slot
    }
    fun getSlot(index : Int) : Slot? = slots[index]

    fun buildInventory(player : Player) : Inventory{
        val inv = Bukkit.createInventory(null, rows * 9, title)
        slots.forEach{
            inv.setItem(it.key, it.value.buildItemStack())
        }
        return inv

    }

    fun onOpen(executor : MenuOpenEvent.() -> Unit){
        onOpenExecutors.add(executor)
    }

    fun open(player : Player){
        val event = MenuOpenEvent(player, this)
        onOpenExecutors.forEach {
            it.invoke(event)
        }

        player.openInventory(this.buildInventory(player))
    }

    fun fill(slot : Slot){
        for(i in 0 until rows*9){
            slots[i] = slot
        }
    }

    fun fill(slot : Slot, vararg indices : Int){
        indices.forEach {
            slots[it] = slot
        }
    }

    fun fillWalls(slot : Slot){
        fillColumn(0, slot)
        fillColumn(8, slot)
    }

    fun fillColumn(column : Int, slot : Slot){
        if(!(0 until 9).contains(column)) return

        var point = column
        for(i in 0 until rows){
            slots[point] = slot
            point += 9
        }
    }

    fun fillRow(row : Int, slot : Slot){
        if(!(0 until rows).contains(row)) return

        for(i in 0 until 9){
            slots[(row * 9) + i] = slot
        }

    }

    fun fillBorders(slot : Slot){
        fillWalls(slot)
        fillRow(0, slot)
        fillRow(rows - 1, slot)
    }


}
@DslMarker
annotation class MenuDSL

@MenuDSL
inline fun menu(builder : Menu.() -> Unit) = Menu().apply(builder)