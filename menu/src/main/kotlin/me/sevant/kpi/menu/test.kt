package me.sevant.kpi.menu

import java.util.*


object JavaHashSetFindMinMaxExample {
    @JvmStatic
    fun main(args: Array<String>) {
        val hSetNumbers: MutableSet<Int> = HashSet()
        hSetNumbers.add(34)
        hSetNumbers.add(13)
        hSetNumbers.add(42)
        hSetNumbers.add(45)
        hSetNumbers.add(12)
        /*
         * Use the max method of Collections class to
         * find max element
         */println("Max value: " + Collections.max(hSetNumbers))
    }
}