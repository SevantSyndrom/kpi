package me.sevant.kpi.command.arg.primitive.numeric

import me.sevant.kpi.command.ArgumentData
import me.sevant.kpi.command.CommandData
import me.sevant.kpi.command.arg.ArgumentType
import me.sevant.kpi.command.arg.CommandSyntaxException
import me.sevant.kpi.utils.KStringReader


class LongArgument(val min : Long = Long.MIN_VALUE, val max : Long = Long.MAX_VALUE) :
    ArgumentType<Long> {

    override fun parse(reader: KStringReader): Long {
        val start = reader.cursor
        val res = reader.readLong()

        if(res < min){
            reader.cursor = start
            throw CommandSyntaxException("Argument was less than the minimum. Found : $res Minimum: $min")
        }
        if(res > max){
            reader.cursor = start
            throw CommandSyntaxException("Argument was more than the maximum. Found : $res Maximum: $max")
        }

        return res
    }
}

fun CommandData.long(arg : ArgumentData) = arg.data as Long