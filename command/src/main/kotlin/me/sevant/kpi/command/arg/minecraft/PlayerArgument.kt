package me.sevant.kpi.command.arg.minecraft

import me.sevant.kpi.command.ArgumentData
import me.sevant.kpi.command.CommandData
import me.sevant.kpi.command.arg.ArgumentType
import me.sevant.kpi.command.arg.CommandSyntaxException
import me.sevant.kpi.utils.KStringReader
import org.bukkit.Bukkit

import org.bukkit.entity.Player

class PlayerArgument : ArgumentType<Player> {

    override fun parse(reader: KStringReader): Player {
        val start = reader.cursor
        val playerName = reader.readString()
        val player = Bukkit.getPlayer(playerName)
        if(player == null){
            reader.cursor = start
            throw(CommandSyntaxException("Could not find $playerName online. Did you spell it correctly?"))
        }
        return player

    }

    override fun getSuggestions(arg: String): Array<String> {
        val suggestions = mutableListOf<String>()
        for(player in Bukkit.getOnlinePlayers()){
            suggestions.add(player.name)
        }
        return suggestions.toTypedArray()
    }
}

fun CommandData.player(arg : ArgumentData) = arg.data as Player