package me.sevant.kpi.command

import com.mojang.brigadier.arguments.ArgumentType
import me.sevant.kpi.command.arg.Argument
import me.sevant.kpi.messages.*
import org.bukkit.command.CommandSender
import java.lang.StringBuilder
import java.util.*
import kotlin.collections.LinkedHashMap
import kotlin.system.measureTimeMillis

class Command {

    var label = ""
    var description = ""
    var permission = ""
    val arguments = linkedMapOf<String, Argument>()
    val children = hashMapOf<String, Command>()
    private val executors = hashSetOf<CommandData.() -> Unit>()
    var parent : Command? = null
        private set

    var helpBuilder : HelpMessageBuilder? = null

    fun hasParent() : Boolean {
        return parent != null
    }

    fun handleCommand(commandData : CommandData) : Boolean{
        if(executors.isEmpty()) return false
        executors.forEach{
            it.invoke(commandData)
        }
        return true
    }

    fun isSubCommand(label : String): Boolean {
        return children.containsKey(label)
    }

    fun executes(executor : CommandData.() -> Unit){
        executors.add(executor)
    }

/*    fun makeUsage() : MessageContent {
        return ArgumentMessageBuilder(this).buildMessage()
    }*/

    fun generateHelp() : HashMap<Message, String>{
        if(helpBuilder != null){
            return helpBuilder!!.message
        }
        return hashMapOf()

    }

    private fun getAllSubCommands() : LinkedList<Command>{
        val queue = LinkedList<Command>()
        val total = LinkedList<Command>()
        queue.add(this)
        total.add(this)
        while(queue.isNotEmpty()){
            val command = queue.poll()
            command.children.forEach{_, com ->
                queue.add(com)
                total.add(com)
            }
        }
        return total

    }

    fun getSuggestions(sender : CommandSender, args : Array<String>) : List<String>{
        val completions = mutableListOf<String>()
        if(args.isEmpty() || args.size == 1) {
            children.forEach {
                completions.add(it.key)
            }
            arguments.values.forEach {
                completions.add(it.label)
            }

        }else{
            var deepest = false
            var curCommand = this
            var a = args
            while(!deepest){
                if(isSubCommand(a.first())){
                    curCommand = curCommand.children[a.first()]!!
                    a = a.drop(1).toTypedArray()

                }else{
                    deepest = true
                }
            }

            curCommand.arguments.forEach{

                if(a.size == 1){
                    completions.addAll(it.value.type.getSuggestions(a.first()).filter {
                        it.toLowerCase().startsWith(a.first().toLowerCase())
                    })

                }
                 a = a.drop(1).toTypedArray()
            }


        }
        return completions
    }

    private fun getDeepestChild(args : Array<String>) : Int{
        var deepest = false
        var current = this
        var a = args
        var toDrop = 0
        while(!deepest){
            if(isSubCommand(a.first())){
                a = a.drop(1).toTypedArray()
                toDrop++
            }else{
                deepest = true
            }
        }
        return toDrop
    }

    fun child(builder : Command.() -> Unit) {
        val command = Command().apply(builder)
        command.parent = this
        children[command.label] = command
    }
}

class CommandData(val sender : CommandSender, val args : LinkedHashMap<String, ArgumentData>)

class ArgumentData(val data : Any?)



inline fun command(builder : Command.() -> Unit) : Command {
    val c = Command()
    c.helpBuilder = HelpMessageBuilder(c)
    c.apply(builder)
    return c
}