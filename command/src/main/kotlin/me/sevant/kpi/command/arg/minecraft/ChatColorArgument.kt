package me.sevant.kpi.command.arg.minecraft

import me.sevant.kpi.command.ArgumentData
import me.sevant.kpi.command.CommandData
import me.sevant.kpi.command.arg.ArgumentType
import me.sevant.kpi.command.arg.CommandSyntaxException
import me.sevant.kpi.utils.KStringReader
import org.bukkit.ChatColor
import java.util.*
import java.util.stream.Stream

class ChatColorArgument : ArgumentType<ChatColor> {

    private val cache : Array<String> by lazy {
        Arrays.stream(ChatColor.values()).map { obj: ChatColor -> obj.name }.toArray{ arrayOf<String>()}
    }

    override fun parse(reader: KStringReader): ChatColor {
        val start = reader.cursor
        val colorText = reader.readString().toUpperCase()
        val color = ChatColor.valueOf(colorText)
        if(color.name != colorText){
            reader.cursor = start
            throw (CommandSyntaxException("Could not parse $colorText as a color!"))
        }
        return color
    }

    override fun getSuggestions(arg: String): Array<String> = cache
}

fun CommandData.color(arg : ArgumentData) = arg.data as ChatColor