package me.sevant.kpi.command.arg.minecraft

import me.sevant.kpi.command.ArgumentData
import me.sevant.kpi.command.CommandData
import me.sevant.kpi.command.arg.ArgumentType
import me.sevant.kpi.command.arg.CommandSyntaxException
import me.sevant.kpi.utils.KStringReader
import org.bukkit.Material
import java.lang.Exception

class MaterialArgument : ArgumentType<Material> {

    val cache : Array<String> by lazy {
        val suggestions = mutableListOf<String>()
        Material.values().forEach { suggestions.add(it.name) }
        suggestions.toTypedArray()
    }

    override fun parse(reader: KStringReader): Material {
        val start = reader.cursor
        //TODO DEBUG

        println(reader.getRemaining())
        reader.cursor = start
        //TODO DEBUG END
        val matString = reader.readString().toUpperCase()
        try {
            return Material.valueOf(matString)
        }catch (ex : Exception){
            reader.cursor = start
            throw(CommandSyntaxException("Failed to parse $matString as a material!"))
        }
    }

    override fun getSuggestions(arg: String): Array<String> {
        return cache
    }
}

fun CommandData.material(arg : ArgumentData) = arg.data as Material