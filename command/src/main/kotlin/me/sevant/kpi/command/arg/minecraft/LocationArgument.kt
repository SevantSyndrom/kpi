package me.sevant.kpi.command.arg.minecraft

import me.sevant.kpi.command.ArgumentData
import me.sevant.kpi.command.CommandData
import me.sevant.kpi.command.arg.ArgumentType
import me.sevant.kpi.command.arg.CommandSyntaxException
import me.sevant.kpi.utils.KStringReader
import org.bukkit.Location
import java.lang.Exception

class LocationArgument : ArgumentType<Location> {

    override fun parse(reader: KStringReader): Location {
        val start = reader.cursor
        try {
            val x = reader.readDouble()
            val y = reader.readDouble()
            val z = reader.readDouble()
            return Location(null, x, y, z)
        }catch (ex : Exception){
            reader.cursor = start
            throw(CommandSyntaxException("Could not parse that as a location. Make sure you do <x y z>"))
        }
    }
}

fun CommandData.location(arg : ArgumentData) = arg.data as Location