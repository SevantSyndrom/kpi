package me.sevant.kpi.command

import me.sevant.kpi.command.arg.Argument
import me.sevant.kpi.command.arg.CommandSyntaxException
import me.sevant.kpi.messages.message
import me.sevant.kpi.messages.sendMessage

import me.sevant.kpi.utils.KStringReader
import me.sevant.kpi.utils.NoStringToParseException
import me.sevant.kpi.utils.StringParseException
import org.bukkit.ChatColor
import org.bukkit.command.CommandSender
import org.bukkit.entity.Player
import org.bukkit.command.Command as BukkitCommand

class CommandExecutor(private val command : Command) : BukkitCommand(command.label, command.description, "", listOf()){


    override fun tabComplete(sender: CommandSender, alias: String, args: Array<String>): MutableList<String> {
        val completions = mutableListOf<String>()
        completions.addAll(command.getSuggestions(sender, args))
        return completions
    }

    override fun execute(sender: CommandSender, label: String, args: Array<out String>): Boolean {

        if(args.isEmpty()){
            handleExecute(KStringReader(""), this.command, sender)
        }else{
            if(args.contains("help") || args.contains("?")){
                val help = command.generateHelp()
                help.forEach {
                    if(sender is Player) {
                        val p = sender as Player
                        if(p.hasPermission(it.value)) p.sendMessage(it.key)
                    }
                }
            }else{
                var a = ""
                for(arg in args){
                    a += "$arg "
                }
                val reader = KStringReader(a)
                val deepest = findDeepestCommand(reader, command)
                handleExecute(reader, deepest, sender)
            }
        }
        return true

    }


    private fun handleExecute(reader : KStringReader, command: Command, sender: CommandSender){
        val argData = linkedMapOf<String, ArgumentData>()
        for(arg in command.arguments){
            try{

                argData[arg.key] = ArgumentData(arg.value.type.parse(reader))
                reader.skipWhiteSpace()
            }catch (ex : CommandSyntaxException){
                if(arg.value.required){
                    buildArgErrorMessage(arg.value, ex).forEach {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', it))

                    }
                    //(sender as Player).sendMessage(message { content.add(command.makeUsage()) })
                    return
                }

            }catch (ex : StringParseException){
                if(arg.value.required){
                    println(ex.msg)
                    return
                }
            }catch (ex : NoStringToParseException){
                if(arg.value.required){
                    buildArgErrorMessage(arg.value, CommandSyntaxException("Argument is required!")).forEach {
                        sender.sendMessage(ChatColor.translateAlternateColorCodes('&', it))

                    }
                    return
                }
            }

        }

        val data = CommandData(sender, argData)
        if(!command.handleCommand(data)){
            command.generateHelp().forEach{
                if(sender is Player){
                    val p = sender as Player
                    if(p.hasPermission(it.value)) p.sendMessage(it.key)
                }
            }
        }
    }

    private fun buildArgErrorMessage(arg : Argument, ex : CommandSyntaxException) : List<String>{
        val list = mutableListOf<String>()
        list.add( "&c&l(!) Error in syntax")
        list.add("&c&lArgument : ${arg.label}   Error: ${ex.msg}")
        return list
    }

    private fun findDeepestCommand(reader : KStringReader, command : Command) : Command{


        var isDeepest = false
        var start = 0
        var current = command
        while(!isDeepest){
            if(!reader.canRead()) return current
            start = reader.cursor
            val res = reader.readString()
            if(!current.isSubCommand(res)){
                isDeepest = true
            }else{
                current = current.children[res]!!
                reader.skipWhiteSpace()
            }

        }
        reader.cursor = start
        reader.skipWhiteSpace()
        return current
    }

}
