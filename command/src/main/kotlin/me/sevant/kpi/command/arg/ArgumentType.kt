package me.sevant.kpi.command.arg

import me.sevant.kpi.utils.KStringReader

interface ArgumentType<T> {


    fun parse(reader : KStringReader) : T

    fun getSuggestions(arg : String) : Array<String> {
        return arrayOf()
    }
    
}