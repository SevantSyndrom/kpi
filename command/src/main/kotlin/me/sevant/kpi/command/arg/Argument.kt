package me.sevant.kpi.command.arg

import me.sevant.kpi.command.Command
import me.sevant.kpi.command.arg.primitive.StringArgument
import org.bukkit.command.CommandException

class Argument(
    var label : String = "",
    var required : Boolean = false,
    var type : ArgumentType<*> = StringArgument(),
    var description : String = ""

)

class CommandSyntaxException(val msg : String) : CommandException() {

}


@DslMarker
annotation class ArgumentDSL

@ArgumentDSL
fun Command.argument(builder : Argument.() -> Unit) {
    val arg = Argument().apply(builder)
    arguments[arg.label] = arg
}