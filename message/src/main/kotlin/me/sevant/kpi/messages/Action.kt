package me.sevant.kpi.messages

enum class Action {
    HOVER_SHOW_TEXT,
    HOVER_SHOW_ITEM,
    HOVER_SHOW_ADVANCEMENT,
    HOVER_SHOW_ENTITY,
    CLICK_OPEN_URL,
    CLICK_OPEN_FILE,
    CLICK_RUN_COMMAND,
    CLICK_SUGGEST_COMMAND,
    CLICK_CHANGE_PAGE
}

data class ActionContent(val action: Action, var data: Any)

fun MessageContent.withAction(action: Action, builder: () -> Any) {
    this.actions.add(ActionContent(action, builder.invoke()))
}


